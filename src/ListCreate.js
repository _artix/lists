import React, { Component } from 'react';
import { Card, CardHeader, InputGroup, InputGroupButton, Input, Button } from 'reactstrap';
import './ListCreate.css';

class ListCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {name: ''};

    this.handleCreateList = this.handleCreateList.bind(this);
    this.handleChangeName = this.handleChangeName.bind(this);
  }

  handleCreateList() {
    this.props.addList(this.state.name);
    this.setState({name: ''});
  }

  handleChangeName(e) {
    this.setState({name: e.target.value});
  }

  render() {

    return (
      <div  className={'ListCreate'}>
      <Card>
        <CardHeader>
          <InputGroup>
            <Input value={this.state.name} onChange={this.handleChangeName} onKeyPress={(e) => (e.key === 'Enter' ? this.handleCreateList() : null)} />
            <InputGroupButton>
              <Button onClick={this.handleCreateList}>+</Button>
            </InputGroupButton>
          </InputGroup>
        </CardHeader>
      </Card>
      </div>
    );
  }
}

export default ListCreate;
