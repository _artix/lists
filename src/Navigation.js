import React, { Component } from 'react';
import { Navbar, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Form, FormGroup, Input, } from 'reactstrap';

class Navigation extends Component {

  render() {
    return (
      <Navbar color={'faded'} style={{flexDirection: 'row'}}>
        <UncontrolledDropdown style={{marginRight: '0.5rem'}}>
          <DropdownToggle>
            Меню
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem header>Header</DropdownItem>
            <DropdownItem disabled>Action</DropdownItem>
            <DropdownItem>Another Action</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Another Action</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <Form>
          <FormGroup style={{margin: 0}}>
            <Input type="search" name="search" id="exampleSearch" />
          </FormGroup>
        </Form>
      </Navbar>
    );
  }
}

export default Navigation;
