import React, { Component } from 'react';
import './App.css';
import Navigation from './Navigation';
import Board from './Board';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigation />
        <Board/>
      </div>
    );
  }
}

export default App;
