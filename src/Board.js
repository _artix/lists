import React, { Component } from 'react';
import { Button,  } from 'reactstrap';
import './Board.css';
import List from './List';
import ListCreate from './ListCreate';
import { connect } from 'react-redux';
import { moveList, addList, deleteList, addItem, deleteItem, moveItem } from './redux/actions';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { compose } from 'redux';
import ListItem from './ListItem';


class Board extends Component {
  render() {
    const {moveList, lists, addList, deleteList, addItem, deleteItem, moveItem} = this.props;
    return (
      <div>
        <div>
          <Button color={'link'}>Ссылка</Button>
          <Button color={'link'}>Ссылка</Button>
          <Button color={'link'}>Ссылка</Button>
        </div>
        <div className={'Board-container'}>
          {lists.map((list, i) => (
            <List
              deleteList={deleteList}
              addItem={addItem}
              id={list.get('id')}
              key={list.get('id')}
              moveList={moveList}
              index={i}
              name={list.get('name')}
            >
              {(list.get('items') && list.get('items').size)
                ? list.get('items').map((item, j) => (
                  <ListItem
                    id={item.get('id')}
                    listId={list.get('id')}
                    deleteItem={deleteItem}
                    moveItem={moveItem}
                    key={j}
                    index={j}
                    listIndex={i}
                  >
                    {item.get('text')}
                  </ListItem>
                ))
                : <ListItem
                  blank
                  id={0}
                  listId={list.get('id')}
                  moveItem={moveItem}
                  index={0}
                  listIndex={i}
                  />}
            </List>
          ))}
          <ListCreate addList={addList} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  lists: state.get('lists')
});

const mapDispatchToProps = dispatch => ({
  moveList: (dragIndex, hoverIndex) => dispatch(moveList(dragIndex, hoverIndex)),
  addList: (name) => dispatch(addList(name)),
  deleteList: (id) => dispatch(deleteList(id)),
  addItem: (listId, text) => dispatch(addItem(listId, text)),
  deleteItem: (listId, itemId) => dispatch(deleteItem(listId, itemId)),
  moveItem: (dragListIndex, dragIndex, hoverListIndex, hoverIndex) => dispatch(moveItem(dragListIndex, dragIndex, hoverListIndex, hoverIndex))
});


const enhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  DragDropContext(HTML5Backend)
);

export default enhance(Board)
