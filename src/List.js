import React, { Component } from 'react';
import { Card, CardBlock, CardHeader, CardFooter, InputGroup, InputGroupButton, Input, Button, ListGroup  } from 'reactstrap';
import { DragSource, DropTarget } from 'react-dnd';
import { findDOMNode } from 'react-dom';
import './List.css';
import { compose } from 'redux';

const listSource = {
  beginDrag(props) {
    return {
      index: props.index
    };
  },
};

const listTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

    // Get horizontal middle
    const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the left
    const hoverClientX = clientOffset.x - hoverBoundingRect.left;

    // Only perform the move when the mouse has crossed half of the items length
    // When dragging forwards, only move when the cursor is further than 50%
    // When dragging backwards, only move when the cursor is closer than 50%

    // Dragging forwards
    if (dragIndex < hoverIndex && hoverClientX < hoverMiddleX) {
      return;
    }

    // Dragging backwards
    if (dragIndex > hoverIndex && hoverClientX > hoverMiddleX) {
      return;
    }

    // Time to actually perform the action
    props.moveList(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  },
};

class List extends Component {

  constructor(props) {
    super(props);
    this.state = {text: ''};

    this.handleDeleteList = this.handleDeleteList.bind(this);
    this.handleCreateItem = this.handleCreateItem.bind(this);
    this.handleChangeText = this.handleChangeText.bind(this);
  }

  handleDeleteList() {
    this.props.deleteList(this.props.id);
  }

  handleCreateItem() {
    this.props.addItem(this.props.id, this.state.text);
    this.setState({text: ''});
  }

  handleChangeText(e) {
    this.setState({text: e.target.value});
  }

  render() {
    const { connectDragSource, isDragging, connectDropTarget, name, connectDragPreview } = this.props;
    const opacity = isDragging ? 0 : 1;

    return connectDragPreview(connectDropTarget(
      <div className={'List'} style={{opacity}}>
        <Card>
          {connectDragSource(<div><CardHeader className={'List-header'}>{name}&#8291;<Button onClick={this.handleDeleteList} className={'List-delete-button'}>X</Button></CardHeader></div>)}
          <CardBlock className={'List-container'}>
            <ListGroup>
              {this.props.children}
            </ListGroup>
          </CardBlock>
          <CardFooter>
          <InputGroup>
            <Input value={this.state.text} onChange={this.handleChangeText} onKeyPress={(e) => (e.key === 'Enter' ? this.handleCreateItem() : null)} />
            <InputGroupButton><Button onClick={this.handleCreateItem}>+</Button></InputGroupButton>
          </InputGroup>
          </CardFooter>
        </Card>
      </div>
    ));
  }
}

const enhance = compose(
  DropTarget('list', listTarget, connect => ({
    connectDropTarget: connect.dropTarget(),
  })),
  DragSource('list', listSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
  })),
);

export default enhance(List)
