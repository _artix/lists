import React, { Component } from 'react';
import { Button, ListGroupItem } from 'reactstrap';
import { compose } from 'redux';
import { DragSource, DropTarget } from 'react-dnd';
import { findDOMNode } from 'react-dom';
import './ListItem.css'

const itemSource = {
  beginDrag(props) {
    return {
      listIndex: props.listIndex,
      index: props.index
    };
  },
};

const itemTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const dragListIndex = monitor.getItem().listIndex;
    const hoverIndex = props.index;
    const hoverListIndex = props.listIndex;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex && dragListIndex === hoverListIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%

    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Time to actually perform the action
    props.moveItem(dragListIndex, dragIndex, hoverListIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
    monitor.getItem().listIndex = hoverListIndex;
  },
};

class ListItem extends Component {
  constructor(props) {
    super(props);
    this.handleDeleteItem = this.handleDeleteItem.bind(this);
  }

  handleDeleteItem() {
    this.props.deleteItem(this.props.listId, this.props.id)
  }

  render() {
    const { connectDragSource, connectDropTarget, blank } = this.props;

    return connectDropTarget(connectDragSource(
      <div style={{minHeight: '50px'}}>
        {!blank && <ListGroupItem className={'ListItem'}>{this.props.children}&#8291;<Button onClick={this.handleDeleteItem} size='sm' className={'ListItem-delete-button'}>X</Button></ListGroupItem>}
      </div>
    ));
  }
}

const enhance = compose(
  DropTarget('item', itemTarget, connect => ({
    connectDropTarget: connect.dropTarget(),
  })),
  DragSource('item', itemSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  })),
);

export default enhance(ListItem)