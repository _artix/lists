import { fromJS } from 'immutable';

const initialState = fromJS({
  lists: ([
    {id: 0, name: 'list1', items: [
      {id: 0, text: 'aaa'},
      {id: 1, text: 'bbb'},
      {id: 2, text: 'ccc'}
    ]},
    {id: 1, name: 'list2', items: ([
      {id: 0, text: 'ddd'},
      {id: 1, text: 'eee'},
      {id: 2, text: 'fff'}
    ])},
    {id: 2, name: 'list3', items: ([
      {id: 0, text: 'ggg'},
      {id: 1, text: 'hhh'},
      {id: 2, text: 'iii'}
    ])},
  ]),
  test: null
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD LIST': {
      const lists = state.get('lists');
      const id = lists.size
        ? lists.map((one) => one.get('id')).reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
        : 0;
      const newLists = lists.push(fromJS({id, name: action.payload}));
      return state.set('lists', newLists);
    }
    case 'MOVE LIST': {
      const lists = state.get('lists');
      const list = lists.get(action.payload.dragIndex);
      const newLists = lists.delete(action.payload.dragIndex).insert(action.payload.hoverIndex, list);
      return state.set('lists', newLists);
    }
    case 'DELETE LIST': {
      const lists = state.get('lists');
      const index = lists.findIndex(elem => elem.get('id') === action.payload);
      const newLists = lists.delete(index);
      return state.set('lists', newLists);
    }
    case 'ADD ITEM': {
      const lists = state.get('lists');
      const index = lists.findIndex(elem => elem.get('id') === action.payload.listId);
      const list = state.get('lists').get(index);
      const items = list.get('items') ? list.get('items') : fromJS([]);

      const id = items.size
        ? items.map((one) => one.get('id')).reduce((prev, cur) => (cur > prev ? cur : prev)) + 1
        : 0;

      const newItems = items.push(fromJS({id, text: action.payload.text}));
      const newList = list.set('items', newItems);
      const newLists = lists.set(index, newList);

      return state.set('lists', newLists);
    }
    case 'DELETE ITEM': {
      const lists = state.get('lists');
      const index = lists.findIndex(elem => elem.get('id') === action.payload.listId);
      const list = state.get('lists').get(index);
      const items = list.get('items');
      const itemIndex = items.findIndex(elem => elem.get('id') === action.payload.itemId);

      const newItems = items.delete(itemIndex);
      const newList = list.set('items', newItems);
      const newLists = lists.set(index, newList);

      return state.set('lists', newLists);
    }
    case 'MOVE ITEM': {
      const lists = state.get('lists');
      const listA = lists.get(action.payload.dragListIndex);
      const itemsA = listA.get('items');
      const item = itemsA.get(action.payload.dragIndex);

      const newItemsA = itemsA.delete(action.payload.dragIndex);
      const newListA = listA.set('items', newItemsA);

      const updatedLists = lists.set(action.payload.dragListIndex, newListA);


      const listB = updatedLists.get(action.payload.hoverListIndex);
      const itemsB = listB.get('items') ? listB.get('items') : fromJS([]);

      const newItemsB = itemsB.insert(action.payload.hoverIndex, item);
      const newListB = listB.set('items', newItemsB);

      const newLists = updatedLists.set(action.payload.hoverListIndex, newListB);

      return state.set('lists', newLists);
    }
    default:
      return state;
  }
};

export {initialState, reducer};