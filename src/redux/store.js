import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import { reducer, initialState } from './reducers';

const enhancer = composeWithDevTools(applyMiddleware());

const store = createStore(
  reducer,
  initialState,
  enhancer
);

export default store;
