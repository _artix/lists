export function moveList(dragIndex, hoverIndex) {
  return {
    type: 'MOVE LIST',
    payload: {dragIndex, hoverIndex}
  };
}

export function addList(name) {
  return {
    type: 'ADD LIST',
    payload: name
  };
}

export function deleteList(id) {
  return {
    type: 'DELETE LIST',
    payload: id
  };
}

export function moveItem(dragListIndex, dragIndex, hoverListIndex, hoverIndex) {
  return {
    type: 'MOVE ITEM',
    payload: {dragListIndex, dragIndex, hoverListIndex, hoverIndex}
  };
}

export function addItem(listId, text) {
  return {
    type: 'ADD ITEM',
    payload: {listId, text}
  };
}

export function deleteItem(listId, itemId) {
  return {
    type: 'DELETE ITEM',
    payload: {listId, itemId}
  };
}
