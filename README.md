# README #

### Этапы создания ###

* При помощи react-create-app создал заготовку программы
* Создаю и верстаю компоненты в статическом виде
* Создаю заготовку redux вещей (папка redux)
* Изучаю react-dnd до определённой степени
* Реализую возможность создавать/удалять листы и пункты
* Реализую возможность перемещать листы и пункты
* Последнии штрихи в вёрстке, уборка мусора в коде

### Примечания ###

* На самом деле границы пунктов довольно размыты, иногда приходилось о чём-то думать наперёд, доделывать или делать параллельно